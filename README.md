Инструкция
----------


# Подготовка сервера
Развертывание системы осуществляется на ОС Ubuntu 14.04
Для развертывания необходимо подготовить :
- предустановленная ОС Ubuntu 14.04
- пользователь user c правами адмнистратора 
- удаленный доступ к серверу по ssh под пользователем user
Имя пользователя и ip адрес в рамках данной инструкции не 
принципиальны, и могут быть любыми допустимыми.


# Установка
Выполнить команды (во время выполнения потребуется утвердительно ответить на заданные вопросы):
sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get install ansible
wget https://bitbucket.org/ddc67cd/osm/raw/master/playbook.yml
sudo ansible-playbook -i "localhost," -c local playbook.yml

Успешным завершением считать отсутствие failed задач:
localhost             : ok=15   changed=15    unreachable=0    failed=0


# Загрузка и импортирование данных в базу postgresql
Подключиться к серверу по ssh, выполнить команды:

1. sudo -i -u osm
2. cd ~/data/
3. wget "<url>"
где <url> - ссылка на файл с данными osm, 
например http://data.gis-lab.info/osm_dump/dump/latest/RU-MO.osm.pbf
Также нужно иметь ввиду, что в ряде случаев (сервер не сообщает имя файла), требуется
явно указывать название загруженного файла с помощью ключа -O. В случае,
если файл уже имеется на диске,
утилита wget переименует загружаемый файл, добавив суффикс-число,
например RU-TY.osm.pbf.1, RU-TY.osm.pbf.2, RU-TY.osm.pbf.3 и тд.
Пример команды: 
wget "http://data.gis-lab.info/osm_dump/dump/latest/RU-YEV.osm.pbf"
4. osm2pgsql -k -d osm -C <cache> --number-processes <procs> <files> 
<cache> - размер оперативной памяти для кеша в мегабайтах, зависит от общего размера памяти в системе
<procs> - количество процессов импортирования, подбираемый параметр
<files> - файлы для импортирования, перечесиление файлов через пробел, или выбор по маске `*.pbf`
Данная команда удалит имеющиеся данные и импортирует новые, из файлов <files>.
Пример команды: 
osm2pgsql -d osm -C 800 --cache-strategy sparse --number-processes 3 RU-YEV.osm.pbf
При импортировании отдельных файлов, можно использовать ключ `-a` утилиты osm2pgsql,
который указывает на то, что необходимо добавить новые данные к уже имеющимся.


# Обновление данных
Обновление данных осуществляется путем удаления имеющихся и 
импортированием новых. Т.е. выполнением команд из раздела `Загрузка и импортирование данных`.


# Генерация тайлов
Подключиться к серверу по ssh, выполнить команды:

1. sudo -i -u osm
2. cd ~/mapnik 
3. python generate_xml.py osm.xml my_osm.xml --dbname osm --accept-none 
4. export MAPNIK_MAP_FILE=~/mapnik/my_osm.xml
5. export MAPNIK_TILE_DIR=~/tiles
6. python generate_tiles.py

В результате в папке /home/osm/mapnik/tiles будет сгенерирован набор тайлов


# Копирование файлов
Копирование осуществляется либо при помощи веб-интерфейса 
хостера (при наличии и поддержке), либо при помощи putty.


## Копирование при помощи putty
Подключиться к серверу по ssh, выполнить команды:

1. sudo -i -u osm
2. tar -cvf <filename> tiles 
<filename> - имя архива
Пример команды:
tar -cvf tiles_01.01.2001.tar.gz


Запустить командную строку в windows, выполнить команду 
pscp.exe user@ip:/home/osm/<filename> "c:\."

В результате файл <filename> будет скопирован в корень диска C



